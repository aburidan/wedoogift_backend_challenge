package services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

import dao.CompanyDao;
import dao.UserDao;
import lombok.RequiredArgsConstructor;
import model.CodeTypeDeposit;
import model.Company;
import model.Deposit;
import model.User;

/**
 * @author aburidant
 * @created 04/04/2022 - 16:44
 * @project wedoogift_backend_challenge
 */
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class UserDepositManagingServicesImpl implements UserDepositManagingServices{

    private final CompanyDao companyDao;
    private final UserDao userDao;

    @Override
    public long calculateUserBalance(long userId) {
        User user = userDao.getById(userId);

        List<Deposit> allDepositList = user.getMealDeposits();
        allDepositList.addAll(user.getGiftDeposits());

        long userBalance = 0L;
        for (Deposit deposit: allDepositList) {
            userBalance = userBalance + deposit.getValue();
        }

        return userBalance;
    }

    @Override
    public boolean giveDepositToUser(CodeTypeDeposit depositType, long depositValue, long companyId, long userId) {
        Company company = companyDao.getById(companyId);
        User user = userDao.getById(userId);

        long companyBalance = company.getBalance();
        if ((companyBalance - depositValue) >= 0) {
            Deposit deposit = new Deposit();
            deposit.setType(depositType);
            deposit.setValue(depositValue);
            deposit.setEmissionDate(LocalDate.now());

            user.getDepositList().add(deposit);

            company.setBalance(companyBalance - depositValue);

            // saving the company will also save the user, which will cascade and save the deposit as well
            companyDao.save(company);

            return true;
        }
        return false;
    }

}

