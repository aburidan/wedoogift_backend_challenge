package services.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author aburidant
 * @created 04/04/2022 - 17:54
 * @project wedoogift_backend_challenge
 */
@Configuration
@ComponentScan(basePackages = "services")
public class ConfigurationScanService {
}
