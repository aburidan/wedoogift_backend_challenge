package model.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author aburidant
 * @created 04/04/2022 - 17:41
 * @project wedoogift_backend_challenge
 */
@Configuration
@EntityScan("model")
public class ConfigurationScanModel {
}
