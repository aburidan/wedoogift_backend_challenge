package model;

import com.sun.istack.NotNull;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

/**
 * @author aburidant
 * @created 04/04/2022 - 16:37
 * @project wedoogift_backend_challenge
 */
@Entity
@Table(name = "USER")
@Getter
@Setter
public class User {

    @Id
    @Column(name = "USER_ID")
    @SequenceGenerator(name = "S_USER", sequenceName = "S_USER", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_USER")
    private Long id;

    @NotNull
    @Column(name = "FIRST_NAME")
    private String lastName;

    @NotNull
    @Column(name = "LAST_NAME")
    private String firstName;

    @NotNull
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "DEPOSIT_ID")
    private List<Deposit> depositList;

    @Transient
    public List<Deposit> getMealDeposits() {
        return this.depositList.stream().filter(deposit -> deposit.isMealDeposit() && deposit.isStillValid()).collect(Collectors.toList());
    }

    @Transient
    public List<Deposit> getGiftDeposits() {
        return this.depositList.stream().filter(deposit -> deposit.isGiftDeposit() && deposit.isStillValid()).collect(Collectors.toList());
    }

    /**
     * This method should only exist in the UserDepositManagingServices class, but I put it here to make the unit tests in ManagingDepositTest class run.
     *
     * @return the calculated user's balance
     */
    @Transient
    public long calculateUserBalance() {
        List<Deposit> allDepositList = this.getMealDeposits();
        allDepositList.addAll(this.getGiftDeposits());

        long userBalance = 0L;
        for (Deposit deposit: allDepositList) {
            userBalance = userBalance + deposit.getValue();
        }

        return userBalance;
    }

}
