package model;

import com.sun.istack.NotNull;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

/**
 * @author aburidant
 * @created 04/04/2022 - 16:37
 * @project wedoogift_backend_challenge
 */
@Entity
@Table(name = "DEPOSIT")
@Getter
@Setter
public class Deposit {

    @Id
    @Column(name = "DEPOSIT_ID")
    @SequenceGenerator(name = "S_DEPOSIT", sequenceName = "S_DEPOSIT", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_DEPOSIT")
    private Long id;

    @NotNull
    @Column(name = "VALUE")
    private long value;

    @NotNull
    @Column(name = "EMISSION_DATE")
    private LocalDate emissionDate;

    @NotNull
    @Column(name = "TYPE")
    protected CodeTypeDeposit type;

    @Transient
    public boolean isMealDeposit() {
        return this.type.equals(CodeTypeDeposit.MEAL);
    }

    @Transient
    public boolean isGiftDeposit() {
        return this.type.equals(CodeTypeDeposit.GIFT);
    }

    /**
     * Indicates whether the deposit is still valid or not, depending on its type (meal or gift)
     *
     * @return true if the deposit can still be used, false otherwise
     */
    @Transient
    public boolean isStillValid() {
        final LocalDate today = LocalDate.now();
        if (isGiftDeposit()) {
            return !today.isAfter(this.emissionDate.plusDays(365));
        } else if (isMealDeposit()) {
            final int currentYear = today.getYear();
            final int emissionYear = this.emissionDate.getYear();
            final boolean isSameYear = emissionYear == currentYear;
            final boolean isTodayBeforeMarchNextYear = (currentYear == emissionYear+1) && (today.getMonthValue() < 3);
            return (isSameYear || isTodayBeforeMarchNextYear);
        } else {
            return false;
        }
    }
}
