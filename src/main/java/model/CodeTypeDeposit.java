package model;

/**
 * @author aburidant
 * @created 04/04/2022 - 16:39
 * @project wedoogift_backend_challenge
 */
public enum CodeTypeDeposit {

    MEAL,

    GIFT,

}
