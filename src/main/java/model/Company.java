package model;

import com.sun.istack.NotNull;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;

/**
 * @author aburidant
 * @created 04/04/2022 - 16:36
 * @project wedoogift_backend_challenge
 */
@Entity
@Table(name = "COMPANY")
@Getter
@Setter
public class Company {

    @Id
    @Column(name = "COMPANY_ID")
    @SequenceGenerator(name = "S_COMPANY", sequenceName = "S_COMPANY", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "S_COMPANY")
    private Long id;

    @NotNull
    @Column(name = "BALANCE")
    private long balance;

    @NotNull
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "USER_ID")
    private List<User> userList;

    /**
     * This method should only exist in the UserDepositManagingServices class, but I put it here to make the unit tests in ManagingDepositTest class run.
     *
     * @param depositType the type of the deposit
     * @param depositValue the amount of the deposit
     * @param user the user the company wants to give the deposit to
     * @return true if everything went well, false otherwise
     */
    @Transient
    public boolean giveDepositToUser(CodeTypeDeposit depositType, long depositValue, User user) {
        long companyBalance = this.getBalance();
        if ((companyBalance - depositValue) >= 0) {
            Deposit deposit = new Deposit();
            deposit.setType(depositType);
            deposit.setValue(depositValue);
            deposit.setEmissionDate(LocalDate.now());

            user.getDepositList().add(deposit);

            this.setBalance(companyBalance - depositValue);

            return true;
        }
        return false;
    }

}
