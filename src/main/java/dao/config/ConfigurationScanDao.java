package dao.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.envers.repository.support.EnversRevisionRepositoryFactoryBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author aburidant
 * @created 04/04/2022 - 17:52
 * @project wedoogift_backend_challenge
 */
@Configuration
@EnableJpaRepositories(basePackages = "dao", repositoryFactoryBeanClass = EnversRevisionRepositoryFactoryBean.class)
public class ConfigurationScanDao {
}
