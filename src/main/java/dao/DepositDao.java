package dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import model.Deposit;

/**
 * @author aburidant
 * @created 04/04/2022 - 16:41
 * @project wedoogift_backend_challenge
 */
@Repository
public interface DepositDao extends JpaRepository<Deposit, Long> {
}
