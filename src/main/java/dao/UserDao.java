package dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import model.User;

/**
 * @author aburidant
 * @created 04/04/2022 - 16:40
 * @project wedoogift_backend_challenge
 */
@Repository
public interface UserDao extends JpaRepository<User, Long> {
}
