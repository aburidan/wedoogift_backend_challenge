package services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;

import java.time.LocalDate;
import java.util.ArrayList;

import dao.CompanyDao;
import dao.UserDao;
import dao.config.ConfigurationScanDao;
import lombok.RequiredArgsConstructor;
import model.CodeTypeDeposit;
import model.Company;
import model.Deposit;
import model.User;
import model.config.ConfigurationScanModel;
import services.config.ConfigurationScanService;

import static org.mockito.Mockito.when;

/**
 *  This unit tests class contains unit tests that don't run.
 *  The real unit tests you might want to run are in the following class : ManagingDepositTest.java
 *  I have had some issues here with the Spring configuration and the beans creation
 *  and I didn't have the time to fix them, but I leave it here so that you can check what I intended to do.
 *
 * @author aburidant
 * @created 04/04/2022 - 16:46
 * @project wedoogift_backend_challenge
 */
@SpringBootTest
@SpringJUnitWebConfig(classes = { ConfigurationScanModel.class, ConfigurationScanDao.class,
        ConfigurationScanService.class })
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@RequiredArgsConstructor(onConstructor_ = @Autowired)
class UserDepositManagingServicesImplTest {

    @MockBean
    private CompanyDao companyDao;

    @MockBean
    private UserDao userDao;

    private final UserDepositManagingServicesImpl userDepositManagingServices;

    private Company createNewCompany() {
        Company company = new Company();
        company.setId(1L);
        company.setBalance(0);
        company.setUserList(new ArrayList<User>());
        return company;
    }

    private User createNewUser() {
        User user = new User();
        user.setId(1L);
        user.setFirstName("Gaston");
        user.setLastName("Lagaffe");
        user.setDepositList(new ArrayList<>());
        return user;
    }

    private Deposit createNewDeposit(long id, long value, CodeTypeDeposit type, LocalDate emissionDate) {
        Deposit deposit = new Deposit();
        deposit.setId(id);
        deposit.setType(type);
        deposit.setValue(value);
        deposit.setEmissionDate(emissionDate);
        return deposit;
    }

    /**
     * Calculates de balance for a user who has 4 deposits : 2 valid deposits and 2 non valid because too old.
     * The expected result is the addiction of the values of the 2 valid deposits.
     */
    @Test()
    void calculateUserBalance() {
        User user = createNewUser();

        // These deposits are still valid today
        Deposit mealDeposit1 = createNewDeposit(1L, 100L, CodeTypeDeposit.MEAL, LocalDate.now().minusDays(10));
        Deposit giftDeposit1 = createNewDeposit(2L, 105L, CodeTypeDeposit.GIFT, LocalDate.now().minusMonths(3));

        // Thses deposits are no longer valid today, they will not be taken into acount for the balance calculation
        Deposit giftDeposit2 = createNewDeposit(3L, 200L, CodeTypeDeposit.GIFT, LocalDate.now().minusDays(400));
        Deposit mealDeposit2 = createNewDeposit(4L, 205L, CodeTypeDeposit.MEAL, LocalDate.now().minusMonths(17));

        user.getDepositList().add(mealDeposit1);
        user.getDepositList().add(giftDeposit1);
        user.getDepositList().add(giftDeposit2);
        user.getDepositList().add(mealDeposit2);

        when(userDao.getById(1L)).thenReturn(user);

        long balance = userDepositManagingServices.calculateUserBalance(1L);
        long expectedBalance = mealDeposit1.getValue() + giftDeposit1.getValue();

        Assertions.assertEquals(expectedBalance, balance);
    }

    /**
     * Gives a user a deposit from a company.
     * The expected result here is a success because de value of the deposit is inferior to the balance of the company.
     * We also expect that the user receives the deposit, and that it is counted in the user's balance.
     */
    @Test()
    void giveDepositToUser_success() {
        User user = createNewUser();
        Company company = createNewCompany();

        company.getUserList().add(user);
        company.setBalance(200L);

        when(userDao.getById(1L)).thenReturn(user);
        when(companyDao.getById(1L)).thenReturn(company);

        when(userDao.save(user)).thenReturn(user);
        when(companyDao.save(company)).thenReturn(company);

        Assertions.assertTrue(userDepositManagingServices.giveDepositToUser(CodeTypeDeposit.GIFT, 100L, 1L, 1L));
        Assertions.assertEquals(100L, company.getBalance());
        Assertions.assertEquals(100L, userDepositManagingServices.calculateUserBalance(1L));
    }

    /**
     * Gives a user 2 deposits from a company.
     * The expected result here is a failure because de value of the second deposit is superior to the balance of the company after the giving of the first deposit.
     * We also expect that the user receives the first deposit, and that it is counted in the user's balance.
     */
    @Test()
    void giveDepositToUser_failure() {
        User user = createNewUser();
        Company company = createNewCompany();

        company.getUserList().add(user);
        company.setBalance(100L);

        when(userDao.getById(1L)).thenReturn(user);
        when(companyDao.getById(1L)).thenReturn(company);

        when(userDao.save(user)).thenReturn(user);
        when(companyDao.save(company)).thenReturn(company);

        Assertions.assertTrue(userDepositManagingServices.giveDepositToUser(CodeTypeDeposit.GIFT, 90L, 1L, 1L));
        Assertions.assertFalse(userDepositManagingServices.giveDepositToUser(CodeTypeDeposit.GIFT, 105L, 1L, 1L));
        Assertions.assertEquals(10L, company.getBalance());
        Assertions.assertEquals(90L, userDepositManagingServices.calculateUserBalance(1L));
    }

}
