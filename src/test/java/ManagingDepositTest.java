import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;

import model.CodeTypeDeposit;
import model.Company;
import model.Deposit;
import model.User;

/**
 * This is the unit tests class we want to run for the test at Wedoogift.
 * I'd rather have had the UserDepositManagingServicesImplTest class run properly but I have had some issues with the Spring configuration and the beans creation.
 * These unit tests can run and pass but I know this is not exactly the very best way to do it.
 */
class ManagingDepositTest {

    private Company createNewCompany() {
        Company company = new Company();
        company.setId(1L);
        company.setBalance(0);
        company.setUserList(new ArrayList<User>());
        return company;
    }

    private User createNewUser() {
        User user = new User();
        user.setId(1L);
        user.setFirstName("Gaston");
        user.setLastName("Lagaffe");
        user.setDepositList(new ArrayList<>());
        return user;
    }

    private Deposit createNewDeposit(long id, long value, CodeTypeDeposit type, LocalDate emissionDate) {
        Deposit deposit = new Deposit();
        deposit.setId(id);
        deposit.setType(type);
        deposit.setValue(value);
        deposit.setEmissionDate(emissionDate);
        return deposit;
    }

    /**
     * Calculates de balance for a user who has 4 deposits : 2 valid deposits and 2 non valid because too old.
     * The expected result is the addiction of the values of the 2 valid deposits.
     */
    @Test()
    void calculateUserBalance() {
        User user = createNewUser();

        // These deposits are still valid today
        Deposit mealDeposit1 = createNewDeposit(1L, 100L, CodeTypeDeposit.MEAL, LocalDate.now().minusDays(10));
        Deposit giftDeposit1 = createNewDeposit(2L, 105L, CodeTypeDeposit.GIFT, LocalDate.now().minusMonths(3));

        // Thses deposits are no longer valid today, they will not be taken into acount for the balance calculation
        Deposit giftDeposit2 = createNewDeposit(3L, 200L, CodeTypeDeposit.GIFT, LocalDate.now().minusDays(400));
        Deposit mealDeposit2 = createNewDeposit(4L, 205L, CodeTypeDeposit.MEAL, LocalDate.now().minusMonths(17));

        user.getDepositList().add(mealDeposit1);
        user.getDepositList().add(giftDeposit1);
        user.getDepositList().add(giftDeposit2);
        user.getDepositList().add(mealDeposit2);

        long balance = user.calculateUserBalance();
        long expectedBalance = mealDeposit1.getValue() + giftDeposit1.getValue();

        Assertions.assertEquals(expectedBalance, balance);
    }

    /**
     * Gives a user a deposit from a company.
     * The expected result here is a success because de value of the deposit is inferior to the balance of the company.
     * We also expect that the user receives the deposit, and that it is counted in the user's balance.
     */
    @Test()
    void giveDepositToUser_success() {
        User user = createNewUser();
        Company company = createNewCompany();

        company.getUserList().add(user);
        company.setBalance(200L);

        Assertions.assertTrue(company.giveDepositToUser(CodeTypeDeposit.GIFT, 100L, user));
        Assertions.assertEquals(100L, company.getBalance());
        Assertions.assertEquals(100L, user.calculateUserBalance());
    }

    /**
     * Gives a user 2 deposits from a company.
     * The expected result here is a failure because de value of the second deposit is superior to the balance of the company after the giving of the first deposit.
     * We also expect that the user receives the first deposit, and that it is counted in the user's balance.
     */
    @Test()
    void giveDepositToUser_failure() {
        User user = createNewUser();
        Company company = createNewCompany();

        company.getUserList().add(user);
        company.setBalance(100L);

        Assertions.assertTrue(company.giveDepositToUser(CodeTypeDeposit.GIFT, 90L, user));
        Assertions.assertFalse(company.giveDepositToUser(CodeTypeDeposit.GIFT, 105L, user));
        Assertions.assertEquals(10L, company.getBalance());
        Assertions.assertEquals(90L, user.calculateUserBalance());
    }

}
